# Specify parent image. Please select a fixed tag here.
ARG BASE_IMAGE=registry.git.rwth-aachen.de/jupyter/profiles/rwth-courses:latest
FROM ${BASE_IMAGE}

# # update conda base environment to match specifications in environment.yml
# ADD environment.yml /tmp/environment.yml

# RUN conda activate base

# # All packages specified in environment.yml are installed in the base environment
# RUN conda env update -f /tmp/environment.yml && \
#     conda clean -a -f -y
RUN pip install -U scikit-learn matplotlib pandas "torch>=1.12" torchvision torchaudio gpytorch